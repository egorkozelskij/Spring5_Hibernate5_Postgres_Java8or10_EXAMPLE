package server.database.service;

import org.springframework.transaction.annotation.Transactional;
import server.database.dao.UserDAO;
import server.database.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    private final UserDAO userDAO;

    @Autowired
    public UserService(UserDAO driversDAO) {
        this.userDAO = driversDAO;
    }

    @Transactional
    public User get() {
        return userDAO.list();
    }
}
