package server.controller;


import server.database.model.User;
import server.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
   private UserService userService;

    @RequestMapping(value = "/guest")
    public String permitAll() {

        User users = userService.get();

        return "TestController: guest access";
    }

}
