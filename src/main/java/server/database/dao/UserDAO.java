package server.database.dao;

import server.database.model.User;

import java.util.List;

public interface UserDAO {
    User list();
}
